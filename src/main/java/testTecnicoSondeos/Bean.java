package testTecnicoSondeos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.AjaxBehaviorEvent;

/**
 * Bean para la l�gica del test
 */
@ManagedBean
@SessionScoped
public class Bean implements Serializable {
    
    private String totalValue = "";

    public String getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(String totalValue) {
        this.totalValue = totalValue;
    }

    private String number = "";

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void siguienteMasGrande() throws AbortProcessingException {
        if(number != null && number.trim().length() > 0){
            String[] numbers = number.trim().split("");
            if (numbers.length <= 1) {
                totalValue = "-1";
            }else{
                Integer[] numbersInt = new Integer[numbers.length];
                for (int i = 0; i < numbers.length; i++) {
                    numbersInt[i]=Integer.valueOf(numbers[i]);
                }
                List<Integer> ordenSiguiente = siguienteMayor(numbersInt);
                String totalString = "";
                for (Integer integer : ordenSiguiente) {
                    totalString +=integer+"";
                }
                if(number.equalsIgnoreCase(totalString)){
                    totalValue = "-1";
                }else{
                    totalValue = totalString;
                }
            }
        }else{
            totalValue = "";
        }
    }
    
    public List<Integer> siguienteMayor(Integer[] num){
        List<Integer> testList=new ArrayList<Integer>();
        int ultimo = num[num.length-1];
        for (int i = num.length-2; i >= 0; i--) {
            int aux;
            if(ultimo > num[i]){
                aux=ultimo;
                ultimo=num[i];
                num[i]=aux;
                break;
            }
        }
        testList.addAll(Arrays.asList(num));
        testList.set(testList.size()-1, ultimo);
        return testList;
    }
}

