package testTecnicoSondeos;

import testTecnicoSondeos.Bean;
import javax.faces.event.AjaxBehaviorEvent;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class BeanTest {
    
    @InjectMocks
    private static Bean bean;
    
    public BeanTest() {
        
    }

    @Test
    public void testSiguienteMasGrande2017() {
        AjaxBehaviorEvent event = null;
        bean.setNumber("2017");
        bean.siguienteMasGrande();
        assertEquals("2071",bean.getTotalValue());
    }
    
    @Test
    public void testSiguienteMasGrande12() {
        AjaxBehaviorEvent event = null;
        bean.setNumber("12");
        bean.siguienteMasGrande();
        assertEquals(bean.getTotalValue(), "21");
    }
    
    @Test
    public void testSiguienteMasGrande513() {
        AjaxBehaviorEvent event = null;
        bean.setNumber("513");
        bean.siguienteMasGrande();
        assertEquals(bean.getTotalValue(), "531");
    }
    
    @Test
    public void testSiguienteMasGrande9() {
        AjaxBehaviorEvent event = null;
        bean.setNumber("9");
        bean.siguienteMasGrande();
        assertEquals(bean.getTotalValue(), "-1");
    }
    
    @Test
    public void testSiguienteMasGrandeVacio() {
        AjaxBehaviorEvent event = null;
        bean.setNumber("");
        bean.siguienteMasGrande();
        assertEquals(bean.getTotalValue(), "");
    }
    
    @Test
    public void testSiguienteMasGrande111() {
        AjaxBehaviorEvent event = null;
        bean.setNumber("111");
        bean.siguienteMasGrande();
        assertEquals("-1",bean.getTotalValue());
    }
    
    @Test
    public void testSiguienteMasGrande531() {
        AjaxBehaviorEvent event = null;
        bean.setNumber("531");
        bean.siguienteMasGrande();
        assertEquals(bean.getTotalValue(), "-1");
    }
    
    @Test
    public void testSiguienteMasGrande1234() {
        AjaxBehaviorEvent event = null;
        bean.setNumber("1234");
        bean.siguienteMasGrande();
        assertEquals("1243",bean.getTotalValue());
    }
}
